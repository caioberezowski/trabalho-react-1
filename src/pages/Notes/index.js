import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    id: ''
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false,
      pesquisa: await listNotes()
    })
  }

  updateText = (event) => {
    this.setState({
        text: event.target.value
    })
}
  updateSearch(event){
    this.setState({search: event.target.value.substr(0,20)});
  }

  searchNote = async (searchTerm) => {

    this.setState({
      pesquisa: this.state.notes.filter(nota => nota.text.toLowerCase().search(searchTerm.target.value.toLowerCase()) !== -1)
    })

  }
  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: '',
      pesquisa: []

    })
  }
  
  delNote = (index) => {
    this.state.notes.splice(index, 1);
    this.setState({
      notes: this.state.notes
    });
  }

  editNote = async (index) => {
    if(index !== -1){
      this.setState({
          text: this.state.notes[index].text,
          id: this.state.notes[index].id,
          index: index
      })
    };
  }

  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>  
            <input type='search' onChange={this.searchNote} placeholder='Procurar...' />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && !this.state.new && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input onChange={this.editText} className='left' type='text' 
                onKeyPress={event => {
                  if (event.key === 'Enter') {
                    this.saveNote()
                  }
                }}  
                placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span className='salvando'>Salvando...</span>) }
                </div>
              </div>
            )}
            
            {
               this.state.notes.map((note, id) => (
              <div className='item' key={note.id}>
                <input onChange={this.editText}  className='left' type='text' defaultValue={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { !note.id && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && (<button onClick={this.delNote.bind(this, note.id)}>Excluir</button>) }
                  { note.id && (<button onClick={() => this.editNote(note.id)} >Editar</button>) }
                </div>
              </div>
            ))
            }

          </div>
        </div>
      </div>
    )
  }
}
